﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.Models
{
    public class Teacher
    {
        [Key]
        public int TeacherId { get; set; }

       
        public string Name { get; set; }

        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        [ForeignKey("School")]
        public int SchoolId { get; set; }
        public virtual Subject Subject { get; set; }

        public virtual School School { get; set; }


    }
}
