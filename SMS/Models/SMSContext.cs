﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.Models
{
    public class SMSContext:DbContext
    {
        public SMSContext(DbContextOptions<SMSContext> options):base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<School> Schools { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
    }
}
