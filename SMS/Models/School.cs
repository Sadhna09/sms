﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.Models
{
    public class School
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Schooladdress { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
       

    }
}
