﻿using Microsoft.EntityFrameworkCore;
using SMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.Repositories
{
    public class SchoolRepository : ISchoolRepository
    {
        private readonly SMSContext _context;
        public SchoolRepository(SMSContext context)
        {
            _context = context;
        }
        //add school
        public async Task<School> Create(School school)
        {
            _context.Schools.Add(school);
            await _context.SaveChangesAsync();

            return school;
        }
        //add teacher
        public async Task<Teacher> Create(Teacher teacher)
        {
            _context.Teachers.Add(teacher);
            await _context.SaveChangesAsync();

            return teacher;
        }


      
        //
        public async Task<IEnumerable<string>> Get(int id)
        {
           
                return await (from x in _context.Schools
                              join y in _context.Teachers on x.Id equals y.SchoolId
                              where x.Id == id
                              select y.Name).ToListAsync();
           
            

           
        }

        public async Task Delete(int id)
        {
            var teacherToDelete = await _context.Teachers.FindAsync(id);
            _context.Teachers.Remove(teacherToDelete);
            await _context.SaveChangesAsync();
        }

       
        public async Task Update(School school)
        {
            _context.Entry(school).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
