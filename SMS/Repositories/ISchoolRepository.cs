﻿using SMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.Repositories
{
    public interface ISchoolRepository
    {

        Task<School> Create(School school);
        Task<IEnumerable<string>> Get(int id);
        Task<Teacher> Create(Teacher teacher);
        
        Task Update(School school);
        Task Delete(int id);
    }
}
